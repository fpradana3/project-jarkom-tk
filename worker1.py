import socket 
import threading
from task import edit_dist_short,edit_distance

HEADER = 64
PORT = 5024
#SERVER = '3.238.134.129'
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "DISCONNECT"

class Server():
    def __init__(self):
        self.list_tugas = []
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(ADDR)

    def handle_client(self,conn, addr):
        print("{} connected.".format(addr))
        while True:
            msg_length = conn.recv(HEADER).decode(FORMAT)
            conn.send((f"Worker [{ADDR}] will do this job\n").encode(FORMAT))
            if msg_length:
                msg_length = int(msg_length)
                msg = conn.recv(msg_length).decode(FORMAT)
                msg = msg.split()
                print(f"[{addr}] Job ({msg[0]}) Received")
                conn.send((f"[{ADDR}] Job ({msg[0]}) Received\n").encode(FORMAT))
                if msg[0] == "disconnect":
                    break
                print(f"[{addr}] Job ({msg[0]}) Running")
                conn.send((f"[{ADDR}] Job ({msg[0]}) Running\n").encode(FORMAT))
                if msg[0] == "edit-dist-long":
                    result = edit_distance(msg[1], msg[2])
                    print(f"[{addr}] {result}")
                elif msg[0] == "edit-dist-short":
                    result = edit_dist_short(msg[1], msg[2])
                    print(f"[{addr}] {result}")
                elif msg[0] == "mul":
                    result = int(msg[1])*int(msg[2])
                    print(f"[{addr}] {result}")
                else:
                    print(f"[{addr}] Job ({msg[0]}) Failed")
                    conn.send((f"[{ADDR}] Job ({msg[0]}) Failed\n").encode(FORMAT))
                    result = "Unknown command"
                print(f"[{addr}] Job ({msg[0]}) Finnished")
                conn.send((f"[{ADDR}] Job ({msg[0]}) Finnished\n").encode(FORMAT))
                conn.send(str(f"Result = {result}").encode(FORMAT))
                # conn.send(("end").encode(FORMAT))
        conn.close()
        
    def start(self):
        self.server.listen()
        print("Server = {}".format(self.server))
        while True:
            try:
                conn, addr = self.server.accept()
                thread = threading.Thread(target=self.handle_client, args=(conn, addr))
                thread.start()
                print("Connection = {}".format(threading.activeCount() - 1))
            except KeyboardInterrupt as e:
                print(f"Shutting down the server...")


if __name__ == "__main__":
    server = Server()
    server.start()