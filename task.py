import numpy as np

def edit_distance(str1,str2):
    table = []
    
    for x in range (len(str2)):
        temp = []
        for y in range (len(str1)):
            temp.append(-1)
        table.append(temp)
    
    arr1 = list(str1)
    arr2 = list(str2)
    for x in range (len(str2)):
        for y in range (len(str1)):
            z = leviathan_distance(table,arr1,arr2,x,y)
    return table[-1][-1]

def leviathan_distance(table,arr1,arr2,i,j):
    if min(i,j) == 0:
        table[i][j] = max(i,j) 
        return max(i,j)
    else:
        if arr1[j] != arr2[i]:
            temp = min(
                leviathan_distance(table,arr1,arr2,i-1,j)+1,
                leviathan_distance(table,arr1,arr2,i,j-1)+1,
                leviathan_distance(table,arr1,arr2,i-1,j-1)+1)
        else:
            temp = min(
                leviathan_distance(table,arr1,arr2,i-1,j)+1,
                leviathan_distance(table,arr1,arr2,i,j-1)+1,
                leviathan_distance(table,arr1,arr2,i-1,j-1))
        table[i][j] = temp
        return temp
        
def edit_dist_short(string_1, string_2):
    # Menginisialisasi matrix berukuan panjang string 1 X panjang string 2
    len_x = len(string_1) + 1 # +1 karena harus disediakan tempat untuk index
    len_y = len(string_2) + 1
    matrix = np.zeros((len_x, len_y))
    
    # Mengisi index string pada matrix
    for x in range(len_x):
        matrix[x, 0] = x
    for y in range(len_y):
        matrix[0, y] = y
        
    for x in range(1, len_x):
        for y in range(1, len_y):
            if min(x, y) == 0:
                matrix[x, y] = max(x, y)
            if string_1[x-1] == string_2[y-1]:
                matrix[x, y] = min(matrix[x-1, y] + 1, 
                                   matrix[x, y-1] + 1, 
                                   matrix[x-1, y-1])
            else:
                matrix[x, y] = min(matrix[x-1, y] + 1, 
                                   matrix[x, y-1] + 1, 
                                   matrix[x-1, y-1] + 1)
                
    return int(matrix[len_x - 1, len_y - 1])