from queue import Queue
import socket
from threading import Thread
import threading
from time import sleep

HEADER = 64
PORT = 5024
FORMAT = 'utf-8'
SERVER = '18.205.66.108'
ADDR = (SERVER, PORT)

class Client():
    def __init__(self):
        self.master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.master.connect(ADDR)
        self.queue = Queue()
    
    def send(self, queue):
        while True:
            message = queue.get()
            message = message.encode(FORMAT)
            msg_length = len(message)
            send_length = str(msg_length).encode(FORMAT)
            send_length += b' ' * (HEADER - len(send_length))
            self.master.send(send_length)
            self.master.send(message)
            if message == "disconnect":
                break
    
    def receive(self):
        while True:
            try:
                message = self.master.recv(2048).decode(FORMAT)
                if message: 
                    print(message)
                else:
                    break
            except:
                break

    def start(self):
        receive = Thread(target=self.receive)
        receive.start()
        while True: 
            print("Command list = \n1. \"edit-dist-short str1 str2\" = Find min edit distance from 2 string\n2. \"edit-dist-long str1 str2\" = Find min edit distance from 2 string \n3. \"mul int1 int2 \"= multiplying 2 integer \n4. \"disconnect\"\n")
            cmnd = input("Your command = \n")
            
            sent = Thread(target=self.send, args=(self.queue,))
            sent.start()
            if cmnd == "disconnect":
                while (self.queue.qsize() != 0):
                    pass
                break
            if len(cmnd.split()) != 3:
                print("Enter correct command")
            else:
                if cmnd.split()[0] == "mul":
                    try:
                        int(cmnd.split()[1])
                        int(cmnd.split()[2])
                    except:
                        print("Enter correct 2 integer number")
                self.queue.put(cmnd)

if __name__ == "__main__":
    master = Client()
    master.start()